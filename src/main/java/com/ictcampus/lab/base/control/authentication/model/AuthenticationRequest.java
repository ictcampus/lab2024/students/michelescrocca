package com.ictcampus.lab.base.control.authentication.model;

import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Value
@Jacksonized
public class AuthenticationRequest {
	String username;
	String password;
}
