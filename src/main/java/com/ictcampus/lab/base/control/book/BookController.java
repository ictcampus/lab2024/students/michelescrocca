package com.ictcampus.lab.base.control.book;

import com.ictcampus.lab.base.control.book.mapper.BookControllerStructMapper;
import com.ictcampus.lab.base.control.book.model.BookResponse;
import com.ictcampus.lab.base.service.book.BookService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
@Slf4j
public class BookController {
	@Autowired
	private BookService bookService;

	@Autowired
	private BookControllerStructMapper bookControllerStructMapper;

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooks() {
		return bookControllerStructMapper.toBooks(bookService.getBooks());
	}

	@GetMapping( value = "/custom", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooksCustom() {
		return bookControllerStructMapper.toBooks(bookService.getBooksCustom());
	}
}
