package com.ictcampus.lab.base.config;

import com.ictcampus.lab.base.config.jwt.JwtRequestFilter;
import com.ictcampus.lab.base.service.user.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Configuration
@EnableMethodSecurity
public class WebSecurityConfig {
	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Bean
	public AuthenticationManager authenticationManager( AuthenticationConfiguration authenticationConfiguration ) throws Exception {
		// Crea e restituisce un oggetto AuthenticationManager utilizzando la configurazione di autenticazione fornita
		return authenticationConfiguration.getAuthenticationManager();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		// Crea e restituisce un oggetto BCryptPasswordEncoder, che sarà utilizzato per codificare le password
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		// Crea un DaoAuthenticationProvider per gestire l'autenticazione
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

		// Configura l'autenticazione per utilizzare il servizio utente personalizzato e il codificatore di password
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());

		return authProvider;
	}

	@Bean
	public JwtRequestFilter jwtRequestFilter() {
		// Crea e restituisce un filtro JWT personalizzato per elaborare le richieste con token JWT
		return new JwtRequestFilter();
	}

	@Bean
	public SecurityFilterChain securityFilterChain( HttpSecurity http ) throws Exception {
		// Configura la catena di filtri di sicurezza HTTP
		http
				.csrf( csrf -> csrf.disable() ) // Disabilita la protezione CSRF
				.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
				// Configura la gestione delle sessioni come senza stato (stateless)
				.authorizeHttpRequests( ( authz ) -> authz
						// Permette l'accesso senza autenticazione a tutte le richieste che corrispondono al pattern "/api/v1/auth/**"
						.requestMatchers( "/api/v1/auth/**" ).permitAll()
						// Richiede l'autenticazione per qualsiasi altra richiesta
						.anyRequest().authenticated()
				);

		// Aggiunge il provider di autenticazione personalizzato
		http.authenticationProvider(authenticationProvider());
		// Aggiunge il filtro JWT prima del filtro UsernamePasswordAuthenticationFilter
		http.addFilterBefore( jwtRequestFilter(), UsernamePasswordAuthenticationFilter.class );

		// Costruisce e restituisce l'oggetto SecurityFilterChain configurato
		return http.build();
	}
}
