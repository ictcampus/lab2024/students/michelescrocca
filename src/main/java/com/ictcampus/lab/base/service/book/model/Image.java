package com.ictcampus.lab.base.service.book.model;

import lombok.Data;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
public class Image {
	private Long id;
	private String title;
	private String url;
	private boolean thumbnail;
}
