package com.ictcampus.lab.base.repository.book;

import com.ictcampus.lab.base.repository.book.entity.BookEntity;

import java.util.List;

public interface BookRepositoryCustom {

    List<BookEntity> findBooksWithExtendData();

}
