package com.ictcampus.lab.base.repository.user.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
@Table(name = "users")
@Entity
public class UserEntity {
	@Id
	@GeneratedValue
	private Long id;

	private String username;
	private String password;
}
