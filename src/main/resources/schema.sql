-- Crea lo schema 'baseproject' se non esiste
CREATE SCHEMA IF NOT EXISTS baseproject;

-- Imposta lo schema corrente a 'baseproject'
SET SCHEMA baseproject;

-- Crea la tabella 'world' con colonne: id, name e system
CREATE TABLE world (
                       id BIGINT AUTO_INCREMENT,                  -- Chiave primaria auto-incrementante
                       name VARCHAR(50) NOT NULL,                 -- Colonna 'name', obbligatoria
                       system VARCHAR(255) NULL                   -- Colonna 'system', opzionale
);

-- Crea un indice unico sulla colonna 'name' della tabella 'world'
CREATE UNIQUE INDEX IDX_WORLD_NAME ON world(name);

-- Crea un indice sulla colonna 'system' della tabella 'world'
CREATE INDEX IDX_WORLD_SYSTEM ON world(system);

-- BIBLIOTECA / LIBRERIA
-- Crea la tabella 'books' con varie colonne per i dettagli dei libri
CREATE TABLE books (
                       id BIGINT AUTO_INCREMENT,                  -- Chiave primaria auto-incrementante
                       title VARCHAR(255) NOT NULL,               -- Colonna 'title', obbligatoria
                       isbn VARCHAR(255) NOT NULL,                -- Colonna 'isbn', obbligatoria
                       abstract VARCHAR(255) NOT NULL,            -- Colonna 'abstract', obbligatoria
                       description CHARACTER LARGE OBJECT NULL,   -- Colonna 'description', opzionale
                       publisher VARCHAR(255) NOT NULL,           -- Colonna 'publisher', obbligatoria
                       published_date DATE NOT NULL,              -- Colonna 'published_date', obbligatoria
                       price FLOAT NULL,                          -- Colonna 'price', opzionale
                       discount FLOAT NULL,                       -- Colonna 'discount', opzionale
                       position_id BIGINT NULL,                   -- Chiave esterna verso la tabella 'positions', opzionale
                       thumbnail_id BIGINT NULL                   -- Chiave esterna verso la tabella 'images', opzionale
);

-- Crea un indice unico sulla colonna 'isbn' della tabella 'books'
CREATE UNIQUE INDEX IDX_BOOKS_ISBN ON books(isbn);

-- Crea un indice sulla colonna 'title' della tabella 'books'
CREATE INDEX IDX_BOOKS_TITLE ON books(title);

-- Crea un indice sulla colonna 'publisher' della tabella 'books'
CREATE INDEX IDX_BOOKS_PUBLISHER ON books(publisher);

-- Crea la tabella 'positions' con colonne per floor, sector, rack e line
CREATE TABLE positions (
                           id BIGINT AUTO_INCREMENT,                  -- Chiave primaria auto-incrementante
                           floor VARCHAR(25) NOT NULL,                -- Colonna 'floor', obbligatoria
                           sector VARCHAR(25) NOT NULL,               -- Colonna 'sector', obbligatoria
                           rack VARCHAR(25) NOT NULL,                 -- Colonna 'rack', obbligatoria
                           line VARCHAR(25) NOT NULL                  -- Colonna 'line', obbligatoria
);

-- Crea un indice unico sulla combinazione delle colonne floor, sector, rack e line nella tabella 'positions'
CREATE UNIQUE INDEX IDX_POSITIONS_POS ON positions(floor, sector, rack, line);

-- Crea la tabella 'authors' con colonne per name, surname, nickname e birthday
CREATE TABLE authors (
                         id BIGINT AUTO_INCREMENT,                  -- Chiave primaria auto-incrementante
                         name VARCHAR(255) NULL,                    -- Colonna 'name', opzionale
                         surname VARCHAR(255) NOT NULL,             -- Colonna 'surname', obbligatoria
                         nickname VARCHAR(255) NULL,                -- Colonna 'nickname', opzionale
                         birthday DATE NULL                         -- Colonna 'birthday', opzionale
);

-- Crea un indice unico sulla colonna 'surname' della tabella 'authors'
CREATE UNIQUE INDEX IDX_AUTHORS_SURNAME ON authors(surname);

-- Crea un indice sulla combinazione delle colonne name, surname e nickname nella tabella 'authors'
CREATE INDEX IDX_AUTHORS_NAME ON authors(name, surname, nickname);

-- Crea la tabella 'images' con colonne per title, url e is_thumbnail
CREATE TABLE images (
                        id BIGINT AUTO_INCREMENT,                  -- Chiave primaria auto-incrementante
                        title VARCHAR(255) NULL,                   -- Colonna 'title', opzionale
                        url VARCHAR(1024) NOT NULL,                -- Colonna 'url', obbligatoria
                        is_thumbnail BOOLEAN NOT NULL DEFAULT false-- Colonna 'is_thumbnail', obbligatoria con valore predefinito false
);

-- Crea un indice unico sulla colonna 'url' della tabella 'images'
CREATE UNIQUE INDEX IDX_IMAGES_URL ON images(url);

-- Crea un indice sulla colonna 'title' della tabella 'images'
CREATE INDEX IDX_IMAGES_TITLE ON images(title);

-- Crea la tabella 'book_author' con colonne per book_id e author_id
CREATE TABLE book_author (
                             book_id BIGINT NOT NULL,                   -- Colonna 'book_id', obbligatoria
                             author_id BIGINT NOT NULL                  -- Colonna 'author_id', obbligatoria
);

-- Crea un indice unico sulla combinazione delle colonne book_id e author_id nella tabella 'book_author'
CREATE UNIQUE INDEX IDX_BOOK_AUTHOR_PK ON book_author(book_id, author_id);

-- Crea la tabella 'book_image' con colonne per book_id e image_id
CREATE TABLE book_image (
                            book_id BIGINT NOT NULL,                   -- Colonna 'book_id', obbligatoria
                            image_id BIGINT NOT NULL                   -- Colonna 'image_id', obbligatoria
);

-- Crea un indice unico sulla combinazione delle colonne book_id e image_id nella tabella 'book_image'
CREATE UNIQUE INDEX IDX_BOOK_IMAGE_PK ON book_image(book_id, image_id);

-- Crea la tabella 'users' con colonne per username e password
CREATE TABLE users (
                       id BIGINT AUTO_INCREMENT,                  -- Chiave primaria auto-incrementante
                       username VARCHAR(30) NOT NULL,             -- Colonna 'username', obbligatoria
                       password VARCHAR(150) NOT NULL             -- Colonna 'password', obbligatoria
);

-- Crea un indice unico sulla colonna 'username' della tabella 'users'
CREATE UNIQUE INDEX IDX_USERS_USERNAME ON users(username);
